package com.unioulu.ssh.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.User;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.Encryption;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

public class SignUpActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    Context context;
    private EditText et_email;
    private EditText et_password;
    private EditText et_confirm_password;
    private EditText et_user_name;
    private String email;
    private String password;
    private String confirm_password;
    private String username;
    private Button button_sign_up;
    ImageView logo;
    private DatabaseReference mDatabase;
    Button button_sign_in;
    User user;
    boolean isUserAlreadyExist = true;
    boolean isSucessfull = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_registration);
        context = this;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        pDialog = new ProgressDialog(context);
        InitUi();
        logo = (ImageView) findViewById(R.id.logo);


    }

    private void InitUi() {
        et_user_name = (EditText) findViewById(R.id.et_user_name);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        button_sign_in = (Button) findViewById(R.id.button_sign_in);
        button_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        button_sign_up = (Button) findViewById(R.id.button_sign_up);
        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = et_email.getText().toString().trim();
                password = et_password.getText().toString().trim();
                confirm_password = et_confirm_password.getText().toString().trim();
                username = et_user_name.getText().toString().trim().toLowerCase();

                if (username.length() == 0) {
                    et_user_name.setError("username");
                } else if (email.length() == 0) {
                    et_email.setError("E-mail empty");
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    et_email.setError("Invalid e-mail");
                } else if (password.length() == 0) {
                    et_password.setError("Password empty");
                } else if (password.length() < 6) {
                    et_password.setError("Password too short, enter minimum 6 characters!");
                } else if (confirm_password.length() == 0) {
                    et_confirm_password.setError("Confirm password");
                } else if ((!password.equals(confirm_password))) {
                    et_confirm_password.setError("Confirmation faild");
                } else if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {
                    showDialog();
                    user = new User(email, Encryption.getEncryptedPassword(password), username, SharedPreferencesHelper.getGcmToken(context));
                    //Value event listener for realtime data update
                    mDatabase.child("users").child(username).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            hideDialog();
                            if (snapshot.exists() && isUserAlreadyExist) {
                                Print.Toast(context, "Username already exist, choose a different username");
                            } else {
                                isUserAlreadyExist = false;
                                mDatabase.child("users").child(username).setValue(user);
                                SharedPreferencesHelper.setLogin(context);
                                SharedPreferencesHelper.setEmail(context, email);
                                SharedPreferencesHelper.setUserName(context, username);
                                SharedPreferencesHelper.setPassword(context, password);
                                if (isSucessfull) {
                                    isSucessfull = false;
                                    Print.Toast(context, "Successfully signed up!");
                                }

                                Intent intent = new Intent(context, MainActivity.class);
                                startActivity(intent);
                                if (LoginActivity.getLoginActivity() != null) {
                                    LoginActivity.getLoginActivity().finish();
                                }
                                finish();
                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            hideDialog();
                            Toast.makeText(context, databaseError.toString(), Toast.LENGTH_LONG).show();
                        }


                    });

                }
            }
        });
    }


    public void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please wait ... ");
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
