package com.unioulu.ssh.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.unioulu.ssh.R;
import com.unioulu.ssh.fragment.HomeFragment;
import com.unioulu.ssh.model.User;
import com.unioulu.ssh.utility.Encryption;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int AlreadySelectedID = -1;
    DrawerLayout drawer;
    LinearLayout ll_home;
    LinearLayout side_menu_logout;
    LinearLayout ll_new_post;
    LinearLayout ll_settings;
    LinearLayout ll_my_post;
    LinearLayout ll_privacy;
    LinearLayout ll_help;
    LinearLayout ll_about;
    Context context;
    Toolbar toolbar;
    Intent intent;
    private ProgressDialog pDialog;
    private DatabaseReference mDatabase;

    public static MainActivity mainActivity = null;

    public static MainActivity getMainActivity() {
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        mainActivity = this;
        pDialog = new ProgressDialog(context);
        InitUi();
        SelectedMenu(R.id.ll_home);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        User user = new User(SharedPreferencesHelper.getEmail(context), Encryption.getEncryptedPassword(SharedPreferencesHelper.getPassword(context)), SharedPreferencesHelper.getUserName(context), SharedPreferencesHelper.getGcmToken(context));
        mDatabase.child("users").child(SharedPreferencesHelper.getUserName(context)).setValue(user);

    }

    /**
     * Initialization of all view component
     */

    private void InitUi() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        ll_home = (LinearLayout) findViewById(R.id.ll_home);
        side_menu_logout = (LinearLayout) findViewById(R.id.side_menu_logout);
        ll_new_post = (LinearLayout) findViewById(R.id.ll_new_post);
        ll_settings = (LinearLayout) findViewById(R.id.ll_settings);
        ll_my_post = (LinearLayout) findViewById(R.id.ll_my_post);
        ll_privacy = (LinearLayout) findViewById(R.id.ll_privacy);
        ll_help = (LinearLayout) findViewById(R.id.ll_help);
        ll_about = (LinearLayout) findViewById(R.id.ll_about);

        ll_home.setOnClickListener(this);
        side_menu_logout.setOnClickListener(this);
        ll_new_post.setOnClickListener(this);
        ll_settings.setOnClickListener(this);
        ll_my_post.setOnClickListener(this);
        ll_privacy.setOnClickListener(this);
        ll_help.setOnClickListener(this);
        ll_about.setOnClickListener(this);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //close drawer on back pressed if it is open

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * This is to change action or activity on sliding menu item click
     *
     * @param selectedmenu view id of the selected menu
     */
    public void SelectedMenu(int selectedmenu) {
        // Do nothing if selectedItem is currentItem
        if (AlreadySelectedID == selectedmenu) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }
        AlreadySelectedID = selectedmenu;
        FragmentManager fm = MainActivity.this.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = null;
        switch (selectedmenu) {
            case R.id.ll_home:
                fragment = new HomeFragment();
                break;

            case R.id.ll_new_post:
                AlreadySelectedID = -1;
                intent = new Intent(context, NewPostActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_settings:
                AlreadySelectedID = -1;
                intent = new Intent(context, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_my_post:
                AlreadySelectedID = -1;
                intent = new Intent(context, MyPostActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_about:
                AlreadySelectedID = -1;
                intent = new Intent(context, AboutActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_help:
                AlreadySelectedID = -1;
                intent = new Intent(context, HelpActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_privacy:
                AlreadySelectedID = -1;
                intent = new Intent(context, PrivacyActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.container, fragment);
            ft.commit();
        }
        drawer.closeDrawer(GravityCompat.START);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_home:
                SelectedMenu(R.id.ll_home);
                break;
            case R.id.side_menu_logout:
                drawer.closeDrawer(GravityCompat.START);
                showLogoutDialog("Are you sure want to logout?");
                break;
            case R.id.ll_new_post:
                SelectedMenu(R.id.ll_new_post);
                break;
            case R.id.ll_settings:
                SelectedMenu(R.id.ll_settings);
                break;
            case R.id.ll_my_post:
                SelectedMenu(R.id.ll_my_post);
                break;
            case R.id.ll_privacy:
                SelectedMenu(R.id.ll_privacy);
                break;
            case R.id.ll_help:
                SelectedMenu(R.id.ll_help);
                break;
            case R.id.ll_about:
                SelectedMenu(R.id.ll_about);
                break;
            default:
                break;

        }
    }

    /**
     * This is to show logout confirmation
     *
     * @param msg Message to show in dialog
     */
    public void showLogoutDialog(String msg) {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(msg);

        Button button_cancel = (Button) dialog.findViewById(R.id.button_cancel);
        Button button_ok = (Button) dialog.findViewById(R.id.button_ok);

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlreadySelectedID = -1;
                SharedPreferencesHelper.logOut(context);
                Intent login_intent = new Intent(context, LoginActivity.class);
                startActivity(login_intent);
                finish();
            }
        });
        dialog.show();

    }

    public void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please wait ... ");
            pDialog.show();
        }
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
