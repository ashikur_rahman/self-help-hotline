package com.unioulu.ssh.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.R;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.BaseFunctions;
import com.unioulu.ssh.utility.Encryption;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements BaseFunctions {

    private ProgressDialog pDialog;
    Context context;
    private EditText et_password;
    private EditText et_user_name;
    private String password;
    private String username;
    private Button button_sign_up;
    ImageView logo;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabase;
    Button button_sign_in;
    public static LoginActivity loginActivity;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    boolean isListeneractivated = false;

    public static LoginActivity getLoginActivity() {
        return loginActivity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        context = this;
        loginActivity = this;
        pDialog = new ProgressDialog(context);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        AskRuntimePermission();
        InitUi();
        logo = (ImageView) findViewById(R.id.logo);


    }

    /**
     * initialization of all the view component of the activity
     */
    private void InitUi() {
        et_user_name = (EditText) findViewById(R.id.et_user_name);
        et_password = (EditText) findViewById(R.id.et_password);
        button_sign_in = (Button) findViewById(R.id.button_sign_in);
        button_sign_up = (Button) findViewById(R.id.button_sign_up);

        button_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Registration = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(Registration);
            }
        });

        button_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isListeneractivated = true;
                password = et_password.getText().toString().trim();
                username = et_user_name.getText().toString().trim().toLowerCase();

                if (username.length() == 0) {
                    et_user_name.setError("Please write your username");
                } else if (password.length() == 0) {
                    et_password.setError("Password cannot be empty");
                } else if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {
                    showDialog();
                    //Value event listener for realtime data update
                    mDatabase.child("users").child(username).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {
                            hideDialog();
                            if (isListeneractivated) { // To stop working this listener when not in this activity
                                isListeneractivated = false;
                                if (snapshot.exists()) {// if user exist in database
                                    try {
                                        //if user credentials match then save the user info in application shared preference
                                        if (Encryption.getEncryptedPassword(password).equals(snapshot.child("password").getValue(String.class))) {
                                            String email = snapshot.child("email").getValue(String.class);
                                            SharedPreferencesHelper.setLogin(context);
                                            SharedPreferencesHelper.setEmail(context, email);
                                            SharedPreferencesHelper.setUserName(context, username);
                                            SharedPreferencesHelper.setPassword(context, password);
                                            Print.Toast(context, "Successfully logged in!");

                                            Intent intent = new Intent(context, MainActivity.class);
                                            startActivity(intent);
                                            finish();

                                        } else {
                                            Toast.makeText(context, "Invalid credentials", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                    Print.Toast(context, "Invalid credentials");
                                }

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            hideDialog();
                            Toast.makeText(context, databaseError.toString(), Toast.LENGTH_LONG).show();
                        }


                    });
                }
            }
        });
    }

    /**
     * asking permission in runtime to use device hardware resources
     */
    private void AskRuntimePermission() {
        if (Build.VERSION.SDK_INT > 22) {
            String internetPermission = "android.permission.INTERNET";
            String cameraPermission = "android.permission.CAMERA";
            String strogePermission = "android.permission.WRITE_EXTERNAL_STORAGE";
            String writestrogePermission = "android.permission.READ_EXTERNAL_STORAGE";
            int hasInternetPermission = checkSelfPermission(internetPermission);
            int hasCameraPermission = checkSelfPermission(cameraPermission);
            int hasStrogePermission = checkSelfPermission(strogePermission);
            int hasWritestrogePermission = checkSelfPermission(writestrogePermission);
            List<String> permissions = new ArrayList();
            if (hasInternetPermission != 0) {
                permissions.add(internetPermission);
            }
            if (hasCameraPermission != 0) {
                permissions.add(cameraPermission);
            }
            if (hasStrogePermission != 0) {
                permissions.add(strogePermission);
            }
            if (hasWritestrogePermission != 0) {
                permissions.add(writestrogePermission);
            }

            if (!permissions.isEmpty()) {

                ActivityCompat.requestPermissions(this, permissions.toArray
                        (new String[permissions.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS:
                if (grantResults[0] != 0) {
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please wait ... ");
            pDialog.show();
        }
    }

    @Override
    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
