package com.unioulu.ssh.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.unioulu.ssh.R;
import com.unioulu.ssh.utility.BaseFunctions;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

public class SettingActivity extends AppCompatActivity implements BaseFunctions {

    private ProgressDialog pDialog;
    Context context;
    Toolbar toolbar;
    Intent intent;
    TextView change_pswd;
    TextView username;
    SwitchCompat switchNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_settings);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        pDialog = new ProgressDialog(context);
        InitUi();


    }

    private void InitUi() {
        change_pswd = (TextView) findViewById(R.id.change_pswd);
        change_pswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(context, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });

        username = (TextView) findViewById(R.id.username);
        username.setText("Username : " + SharedPreferencesHelper.getUserName(context));
        switchNotification = (SwitchCompat) findViewById(R.id.switchNotification);
        switchNotification.setChecked(SharedPreferencesHelper.getNotification(context));
        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferencesHelper.setNotification(context, isChecked);
                if (isChecked)
                    Print.Toast(context, "Notification on");
                else
                    Print.Toast(context, "Notification off");

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    @Override
    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
