package com.unioulu.ssh.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.R;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.BaseFunctions;
import com.unioulu.ssh.utility.Encryption;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity extends AppCompatActivity implements BaseFunctions {

    private ProgressDialog pDialog;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference mDatabase;
    private Context context;
    private Toolbar toolbar;
    private Button activate;
    private EditText oldpsswd;
    private EditText newPsswd;
    private EditText dublicatePsswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.change_password);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        pDialog = new ProgressDialog(context);
        initUi();


    }

    /**
     * Initialize ui elements
     */
    private void initUi() {
        activate = (Button) findViewById(R.id.activatePassword);
        oldpsswd = (EditText) findViewById(R.id.oldPassword);
        newPsswd = (EditText) findViewById(R.id.newPassword);
        dublicatePsswd = (EditText) findViewById(R.id.dublicatePassword);
        activate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldPassword = oldpsswd.getText().toString().trim();
                String newPassword = newPsswd.getText().toString().trim();
                String confirmPassword = dublicatePsswd.getText().toString().trim();

                if (oldPassword.length() == 0) {
                    oldpsswd.setError("Enter old password");
                } else if (!SharedPreferencesHelper.getPassword(context).equals(oldPassword)) {
                    oldpsswd.setError("Old password is not correct");
                } else if (newPassword.length() == 0) {
                    newPsswd.setError("Enter new password");
                } else if (newPassword.length() < 6) {
                    newPsswd.setError("Password too short, enter minimum 6 characters!");
                } else if (confirmPassword.length() == 0) {
                    dublicatePsswd.setError("Confirm password");
                } else if ((!newPassword.equals(confirmPassword))) {
                    dublicatePsswd.setError("Confirmation faild");
                } else if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {
                    showDialog();

                    mDatabase.child("users").child(SharedPreferencesHelper.getUserName(context)).child("password").setValue(Encryption.getEncryptedPassword(newPassword));
                    SharedPreferencesHelper.setPassword(context, newPassword);
                    hideDialog();
                    Print.Toast(context, "Password has been updated!");
                    ChangePasswordActivity.this.finish();
                }


            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    @Override
    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
