package com.unioulu.ssh.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.widget.LinearLayout;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.unioulu.ssh.R;
import com.unioulu.ssh.adapter.PostAdapter;
import com.unioulu.ssh.model.Post;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.BaseFunctions;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.RecyclerViewEmptySupport;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class MyPostActivity extends AppCompatActivity implements BaseFunctions {

    private ProgressDialog pDialog;
    Context context;
    Toolbar toolbar;

    private DatabaseReference mDatabase;
    private RecyclerViewEmptySupport recyclerView;
    PostAdapter postAdapter;
    List<Post> postList = new ArrayList<>();
    RecyclerView.LayoutManager mLayoutManager;
    LinearLayout parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_my_post);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        pDialog = new ProgressDialog(context);
        initUi();

    }

    /**
     * Get post all post for currently logged in user
     */
    private void getPostsByUserName() {
        postList.clear();
        //showDialog();
        mDatabase.child("post").orderByChild("username").equalTo(SharedPreferencesHelper.getUserName(context)).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                // hideDialog();
                Post post = dataSnapshot.getValue(Post.class);
                if (post.isPublished()) {
                    postList.add(post);
                    Collections.sort(postList, new Comparator<Post>() {
                        @Override
                        public int compare(Post o1, Post o2) {
                            return new Date(o2.getAdd_date()).compareTo(new Date(o1.getAdd_date()));
                        }
                    });
                    postAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * set user post data in list after getting form server
     */
    private void setListData() {
        postAdapter = new PostAdapter(postList);
        mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setEmptyView(findViewById(R.id.list_empty));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postAdapter);

    }

    private void initUi() {
        recyclerView = (RecyclerViewEmptySupport) findViewById(R.id.recycler_view);
        parent = (LinearLayout) findViewById(R.id.parent);
        Print.SnackBar(parent, "Long tap item to edit your post");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetInfo.isOnline(context)) {
            mDatabase = FirebaseDatabase.getInstance().getReference();
            setListData();
            getPostsByUserName();
        } else {
            AlertMessage.showMessage(context, "Error", AllStactic.netWarning);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please wait ... ");
            pDialog.show();
        }
    }

    @Override
    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
