package com.unioulu.ssh.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.Category;
import com.unioulu.ssh.model.Post;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class NewPostActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    Context context;
    Toolbar toolbar;
    private ImageView Im_imagePicker;
    private EditText et_post_title;
    private EditText et_post_details;
    private Spinner category_list;
    Button buttonPost;
    private DatabaseReference mDatabase;
    String title;
    String details;
    Category category;
    private ArrayList<Category> allCategory = new ArrayList<>();
    private static int RESULT_LOAD_IMAGE = 1;
    String picturePath = "";
    String postId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_post);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        pDialog = new ProgressDialog(context);
        InitUi();


    }

    /**
     * populate spinner with the category name that are already from server
     */
    public void PopulateCategorySpinner() {
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, R.layout.simple_spinner_item, allCategory);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        category_list.setAdapter(adapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bmp = BitmapFactory.decodeFile(picturePath);
            Im_imagePicker.setImageBitmap(bmp);
            bmp = null;

        }
    }

    /**
     * initialization of the all view components and validation of new post data availability
     */
    private void InitUi() {
        Im_imagePicker = (ImageView) findViewById(R.id.Im_imagePicker);
        Im_imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        et_post_title = (EditText) findViewById(R.id.et_post_title);
        et_post_details = (EditText) findViewById(R.id.et_post_details);
        et_post_details.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Nothing
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 600) {
                    // Trim words to length MAX_WORDS
                    // Join words into a String
                    et_post_details.setError("Maximum limit of letters has crossed");
                    et_post_details.setText(s.subSequence(0, 599));
                    et_post_details.setSelection(et_post_details.getText().length());

                }
            }
        });
        category_list = (Spinner) findViewById(R.id.category_list);
        category_list.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        buttonPost = (Button) findViewById(R.id.buttonPost);


        // send new post to server
        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title = et_post_title.getText().toString().trim();
                details = et_post_details.getText().toString().trim();
                category = allCategory.get(category_list.getSelectedItemPosition());

                if (title.length() == 0) {
                    et_post_title.setError("Write a title");
                } else if (details.length() == 0) {
                    et_post_details.setError("Write details ");
                } else if (category_list.getSelectedItemPosition() == 0) {
                    Print.Toast(context, "Select a category");
                } else if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {
                    (new AsyncTask() {

                        @Override
                        protected Object doInBackground(Object[] params) {
                            try {

                                postId = UUID.randomUUID().toString();

                                Map<String, Object> value = new HashMap<>();
                                value.put("id", postId);
                                value.put("add_date", ServerValue.TIMESTAMP);
                                value.put("reported", false);
                                value.put("published", true);
                                value.put("category", category.getId());
                                value.put("details", details);
                                value.put("last_updated", ServerValue.TIMESTAMP);
                                value.put("title", title);
                                value.put("username", SharedPreferencesHelper.getUserName(context));
                                if (!picturePath.equals("") && picturePath != null) {
                                    value.put("image", getBase64Image(picturePath));
                                } else {
                                    value.put("image", "");
                                }

                                mDatabase.child("post").child(postId).setValue(value);

                            } catch (ExceptionInInitializerError e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            return postId;
                        }

                        protected void onPreExecute() {
                            super.onPreExecute();
                            showDialog();

                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            mDatabase.child("post").child(postId).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    hideDialog();
                                    NewPostActivity.this.finish();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    hideDialog();
                                    Toast.makeText(context, databaseError.toString(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }


                    }).execute();


                }
            }
        });


        mDatabase.child("category").

                addValueEventListener(new ValueEventListener() {
                                          @Override
                                          public void onDataChange(DataSnapshot dataSnapshot) {
                                              Print.log(dataSnapshot);
                                              Category categry;
                                              categry = new Category();
                                              categry.setTitle("Choose a category");
                                              allCategory.add(categry);
                                              for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                  categry = postSnapshot.getValue(Category.class);
                                                  categry.setSearchKey(categry.getTitle());
                                                  allCategory.add(categry);
                                              }
                                              PopulateCategorySpinner();

                                          }

                                          @Override
                                          public void onCancelled(DatabaseError databaseError) {
                                              Print.Toast(context, databaseError.toString());
                                          }
                                      }

                );
    }

    /**
     * get image in base64 format
     * @param path of the imahe
     * @return
     */
    public String getBase64Image(String path) {
        File imgFile = new File(path);
        String image = "";

        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                    .getAbsolutePath());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 20,
                    byteArrayOutputStream);
            myBitmap.recycle();

            byte[] byteArray = byteArrayOutputStream.toByteArray();
            image = Base64.encodeToString(byteArray, Base64.DEFAULT);
            byteArray = null;
        }
        return image;
    }

    /**
     * Create confirmation dialog if there is any data in the form
     */
    public void showDialog(String msg) {
        final Dialog dialog = new Dialog(NewPostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(msg);

        Button button_cancel = (Button) dialog.findViewById(R.id.button_cancel);
        Button button_ok = (Button) dialog.findViewById(R.id.button_ok);

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                NewPostActivity.this.finish();
            }
        });
        dialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!et_post_details.getText().toString().trim().equals("") || !et_post_title.getText().toString().trim().equals("")) {
                    showDialog("Are you sure to discard?");
                } else {
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please wait ... ");
            pDialog.show();
        }
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (!et_post_details.getText().toString().trim().equals("") || !et_post_title.getText().toString().trim().equals("")) {
            showDialog("Are you sure to discard?");
        } else {
            finish();
        }
    }
}
