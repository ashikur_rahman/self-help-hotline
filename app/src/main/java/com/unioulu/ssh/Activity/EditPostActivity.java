package com.unioulu.ssh.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.Category;
import com.unioulu.ssh.model.Post;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditPostActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    Context context;
    Toolbar toolbar;
    private ImageView Im_imagePicker;
    private EditText et_post_title;
    private EditText et_post_details;
    private Spinner category_list;
    Button buttonPost;
    private DatabaseReference mDatabase;
    String title;
    String details;
    Category category;
    private ArrayList<Category> allCategory = new ArrayList<>();
    private static int RESULT_LOAD_IMAGE = 1;
    String picturePath;
    String postId = "";
    Post postDetails;
    boolean updating = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_new_post);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        pDialog = new ProgressDialog(context);
        postDetails = (Post) getIntent().getSerializableExtra("PostDetails");
        picturePath = "";
        if (!NetInfo.isOnline(context)) {
            AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
        } else {
            mDatabase = FirebaseDatabase.getInstance().getReference();
            InitUi();

        }


    }


    /**
     * initialization of the all view components and validation of new post data availability
     */
    private void InitUi() {
        Im_imagePicker = (ImageView) findViewById(R.id.Im_imagePicker);
        Im_imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        et_post_title = (EditText) findViewById(R.id.et_post_title);
        et_post_details = (EditText) findViewById(R.id.et_post_details);
        et_post_details.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                // Nothing
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 600) {
                    // Trim words to length MAX_WORDS
                    // Join words into a String
                    et_post_details.setError("Maximum limit of letters has crossed");
                    et_post_details.setText(s.subSequence(0, 599));
                    et_post_details.setSelection(et_post_details.getText().length());

                }
            }
        });
        category_list = (Spinner) findViewById(R.id.category_list);
        category_list.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        buttonPost = (Button) findViewById(R.id.buttonPost);
        buttonPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title = et_post_title.getText().toString().trim();
                details = et_post_details.getText().toString().trim();
                category = allCategory.get(category_list.getSelectedItemPosition());

                if (title.length() == 0) {
                    et_post_title.setError("Write a title");
                } else if (details.length() == 0) {
                    et_post_details.setError("Write details ");
                } else if (category_list.getSelectedItemPosition() == 0) {
                    Print.Toast(context, "Select a category");
                } else if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {
                    (new AsyncTask() {

                        @Override
                        protected Object doInBackground(Object[] params) {
                            try {

                                postId = postDetails.getId();

                                Map<String, Object> value = new HashMap<>();
                                value.put("id", postDetails.getId());
                                value.put("add_date", postDetails.getAdd_date());
                                value.put("reported", postDetails.isReported());
                                value.put("published", postDetails.isPublished());
                                value.put("category", category.getId());
                                value.put("details", details);
                                value.put("last_updated", ServerValue.TIMESTAMP);
                                value.put("title", title);
                                value.put("username", SharedPreferencesHelper.getUserName(context));
                                if (!picturePath.equals("") || picturePath != null) {
                                    value.put("image", getBase64Image(picturePath));
                                } else {
                                    value.put("image", postDetails.getPhoto());
                                }

                                mDatabase.child("post").child(postId).setValue(value);

                            } catch (ExceptionInInitializerError e) {
                                e.printStackTrace();
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                            return "";
                        }

                        protected void onPreExecute() {
                            super.onPreExecute();
                            showDialog();

                        }

                        @Override
                        protected void onPostExecute(Object o) {
                            mDatabase.child("post").child(postId).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    hideDialog();
                                    if (updating) {
                                        updating = false;
                                        Print.Toast(context, "Post Updated");
                                    }
                                    EditPostActivity.this.finish();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    hideDialog();
                                    Toast.makeText(context, databaseError.toString(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }


                    }).execute();


                }
            }
        });

// get post category from server
        mDatabase.child("category").

                addValueEventListener(new ValueEventListener() {
                                          @Override
                                          public void onDataChange(DataSnapshot dataSnapshot) {
                                              Print.log(dataSnapshot);
                                              Category categry;
                                              categry = new Category();
                                              categry.setTitle("Choose a category");
                                              allCategory.add(categry);  // add an extra elements to dropdown as hint
                                              for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                                  categry = postSnapshot.getValue(Category.class);
                                                  categry.setSearchKey(categry.getTitle());
                                                  allCategory.add(categry);
                                              }
                                              PopulateCategorySpinner();

                                          }

                                          @Override
                                          public void onCancelled(DatabaseError databaseError) {
                                              Print.Toast(context, databaseError.toString());
                                          }
                                      }

                );
// get post photo from server
        mDatabase.child("post").child(postDetails.getId()).child("image").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    postDetails.setPhoto(dataSnapshot.getValue().toString());
                }
                Print.log(dataSnapshot);
                if (!postDetails.getPhoto().equals("") && postDetails.getPhoto() != null) {
                    byte[] imageByteArray = Base64.decode(postDetails.getPhoto(), Base64.DEFAULT);
                    Glide.with(EditPostActivity.this).load(imageByteArray).placeholder(R.drawable.ic_launcher).crossFade().fitCenter().into(Im_imagePicker);
                    imageByteArray = null;

                } else {
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        et_post_title.setText(postDetails.getTitle());
        et_post_details.setText(postDetails.getDetails());
        buttonPost.setText("Update");


    }

    /**
     * populate spinner with the category name that are already from server
     */
    public void PopulateCategorySpinner() {
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, R.layout.simple_spinner_item, allCategory);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        category_list.setAdapter(adapter);
        category_list.setSelection(getCategoryPosition());
    }

    /**
     * find the category position of the selected post
     * @return
     */
    private int getCategoryPosition() {
        int index = 0;
        for (int i = 0; i < allCategory.size(); i++) {
            if (allCategory.get(i).getId() == postDetails.getCategory()) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bmp = BitmapFactory.decodeFile(picturePath);
            Im_imagePicker.setImageBitmap(bmp);

        }
    }

    public String getBase64Image(String path) {
        File imgFile = new File(path);
        String image = "";

        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                    .getAbsolutePath());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 20,
                    byteArrayOutputStream);
            myBitmap.recycle();

            byte[] byteArray = byteArrayOutputStream.toByteArray();
            image = Base64.encodeToString(byteArray, Base64.DEFAULT);
            byteArray = null;
        }
        return image;
    }

    /**
     * Create confirmation dialog if there is any data in the form
     */
    public void showDialog(String msg) {
        final Dialog dialog = new Dialog(EditPostActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText(msg);

        Button button_cancel = (Button) dialog.findViewById(R.id.button_cancel);
        Button button_ok = (Button) dialog.findViewById(R.id.button_ok);

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                EditPostActivity.this.finish();
            }
        });
        dialog.show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!et_post_details.getText().toString().trim().equals("") || !et_post_title.getText().toString().trim().equals("")) {
                    showDialog("Are you sure to discard?");
                } else {
                    finish();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (!et_post_details.getText().toString().trim().equals("") || !et_post_title.getText().toString().trim().equals("")) {
            showDialog("Are you sure to discard?");
        } else {
            finish();
        }
    }
}
