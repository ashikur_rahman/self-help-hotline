package com.unioulu.ssh.Activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.unioulu.ssh.R;
import com.unioulu.ssh.adapter.CommentAdapter;
import com.unioulu.ssh.model.Comment;
import com.unioulu.ssh.model.CommentReply;
import com.unioulu.ssh.model.Post;
import com.unioulu.ssh.model.User;
import com.unioulu.ssh.model.Vote;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.AppController;
import com.unioulu.ssh.utility.BaseFunctions;
import com.unioulu.ssh.utility.DateUtil;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.RecyclerViewEmptySupport;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PostDetailsActivity extends AppCompatActivity implements BaseFunctions {

    private RecyclerViewEmptySupport recyclerView;
    Context context;
    Toolbar toolbar;
    Post postDetails;
    TextView postHeadline;
    TextView postText;
    TextView postUser;
    TextView timestamp;
    TextView post_username;
    CommentAdapter commentAdapter;
    public List<Comment> commentList = new ArrayList<Comment>();
    RecyclerView.LayoutManager mLayoutManager;
    Button post_report;
    LinearLayout like_unlike_container;
    Button button_send;
    EditText et_comment;
    private DatabaseReference mDatabase;
    LinearLayout ll_like_post;
    LinearLayout ll_unlike_post;
    ImageView im_like, im_unlike;
    TextView tv_like, tv_unlike;
    ProgressDialog pDialog;
    TextView button_comment;
    RelativeLayout rl_bottom_container;
    public static PostDetailsActivity postDetailsActivity;
    public boolean isKeyboardOpenforComment = false;
    public boolean isKeyboardOpenforReply = false;
    Comment commentToReply;
    ImageView im_post;
    RelativeLayout rl_image;

    public static PostDetailsActivity getPostDetailsActivity() {
        return postDetailsActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_post_details);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        postDetailsActivity = this;
        pDialog = new ProgressDialog(context);
        try {
            postDetails = (Post) getIntent().getSerializableExtra("PostDetails");

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!NetInfo.isOnline(context)) {
            AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
        } else {
            mDatabase = FirebaseDatabase.getInstance().getReference();
            InitUi();
            getComments();
        }
    }


    private void InitUi() {
        rl_image = (RelativeLayout) findViewById(R.id.rl_image);
        im_post = (ImageView) findViewById(R.id.im_post);
        rl_bottom_container = (RelativeLayout) findViewById(R.id.rl_bottom_container);
        tv_like = (TextView) findViewById(R.id.tv_like);
        tv_unlike = (TextView) findViewById(R.id.tv_unlike);
        im_like = (ImageView) findViewById(R.id.im_like);
        im_unlike = (ImageView) findViewById(R.id.im_unlike);
        ll_like_post = (LinearLayout) findViewById(R.id.ll_like_post);
        ll_unlike_post = (LinearLayout) findViewById(R.id.ll_unlike_post);

        et_comment = (EditText) findViewById(R.id.et_comment);
        post_username = (TextView) findViewById(R.id.post_username);
        recyclerView = (RecyclerViewEmptySupport) findViewById(R.id.recycler_view_comment);
        postHeadline = (TextView) findViewById(R.id.post_headline);
        postText = (TextView) findViewById(R.id.post_details_text);
        postUser = (TextView) findViewById(R.id.post_username);
        timestamp = (TextView) findViewById(R.id.post_timestamp);
        post_report = (Button) findViewById(R.id.post_report);
        like_unlike_container = (LinearLayout) findViewById(R.id.like_unlike_container);

        // if the post is a my post hide report and up/down vote
        if (postDetails.getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
            post_report.setText("Delete");
            like_unlike_container.setVisibility(View.GONE);
        } else {
            post_report.setText("Report");
            if (postDetails.getIsliked() == 0) {
                im_like.setAlpha(.2f);
                im_unlike.setAlpha(.2f);
            } else if (postDetails.getIsliked() == 1) {
                im_like.setAlpha(1f);
                im_unlike.setAlpha(.2f);
            } else if (postDetails.getIsliked() == -1) {
                im_like.setAlpha(.2f);
                im_unlike.setAlpha(1f);
            }
        }
        ll_like_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> value = new HashMap<>();
                if (postDetails.getIsliked() == 1) {
                    value.put("like", 0);
                    postDetails.setIsliked(0);
                    postDetails.setLike(postDetails.getLike() - 1);

                    im_like.setAlpha(.2f);
                    im_unlike.setAlpha(.2f);

                } else {
                    value.put("like", 1);
                    im_like.setAlpha(1f);
                    im_unlike.setAlpha(.2f);
                    postDetails.setLike(postDetails.getLike() + 1);
                    if (postDetails.getIsliked() == -1) {
                        postDetails.setUnlike(postDetails.getUnlike() - 1);
                    }
                    postDetails.setIsliked(1);

                }
                mDatabase.child("post").child(postDetails.getId()).child("votes").child(SharedPreferencesHelper.getUserName(context)).setValue(value);
                setLikeUnlike();
            }
        });

        ll_unlike_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> value = new HashMap<>();
                if (postDetails.getIsliked() == -1) {
                    value.put("like", 0);
                    postDetails.setIsliked(0);
                    im_like.setAlpha(.2f);
                    im_unlike.setAlpha(.2f);
                    postDetails.setUnlike(postDetails.getUnlike() - 1);

                } else {
                    value.put("like", -1);
                    im_like.setAlpha(.2f);
                    im_unlike.setAlpha(1f);
                    postDetails.setUnlike(postDetails.getUnlike() + 1);
                    if (postDetails.getIsliked() == 1) {
                        postDetails.setLike(postDetails.getLike() - 1);
                    }
                    postDetails.setIsliked(-1);
                }
                mDatabase.child("post").child(postDetails.getId()).child("votes").child(SharedPreferencesHelper.getUserName(context)).setValue(value);
                setLikeUnlike();
            }
        });
        // set value
        postHeadline.setText(postDetails.getTitle());
        postText.setText(postDetails.getDetails());
        postUser.setText(postDetails.getUsername());
        timestamp.setText(DateUtil.relativeDate(postDetails.getAdd_date()));
        post_username.setText(postDetails.getUsername());
        button_send = (Button) findViewById(R.id.button_send);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String commentString = et_comment.getText().toString().trim();
                if (commentString.equals("")) {
                    if (isKeyboardOpenforComment) {
                        et_comment.setError("Write your comment");
                    } else if (isKeyboardOpenforReply) {
                        et_comment.setError("Write your reply");
                    }
                } else if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {

                    et_comment.setText(""); // reset comment to blank
                    // hide keyboard
                    View view = PostDetailsActivity.this.getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);
                    }
                    hideKeyboard();
                    recyclerView.scrollToPosition(commentList.size() - 1); //scroll the list to last item

                    String commentId = UUID.randomUUID().toString();
                    Map<String, Object> value = new HashMap<>();

                    //add a comment to a post
                    if (isKeyboardOpenforComment) {
                        isKeyboardOpenforComment = false;
                        isKeyboardOpenforReply = false;
                        value.put("id", commentId);
                        value.put("add_date", ServerValue.TIMESTAMP);
                        value.put("username", SharedPreferencesHelper.getUserName(context));
                        value.put("comment", commentString);
                        value.put("postId", postDetails.getId());
                        value.put("is_best_answer", false);
                        showDialog();

                        mDatabase.child("comments").child(commentId).setValue(value);
                        if (!postDetails.getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
                            mDatabase.child("comments").child(commentId).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    mDatabase.child("users").child(postDetails.getUsername()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            User user = dataSnapshot.getValue(User.class);
                                            // implement notification here
                                            String notificationText = "An user has commented on your post \n\"" + postDetails.getTitle() + "\"";
                                            SendRequestNotificationToUser(user, notificationText);

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });


                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    hideDialog();
                                    Toast.makeText(context, databaseError.toString(), Toast.LENGTH_LONG).show();
                                }
                            });

                        } else {
                            hideDialog();
                        }

                    }
                    // add reply to a comment
                    else if (isKeyboardOpenforReply) {
                        isKeyboardOpenforComment = false;
                        isKeyboardOpenforReply = false;

                        value.put("id", commentId);
                        value.put("add_date", ServerValue.TIMESTAMP);
                        value.put("username", SharedPreferencesHelper.getUserName(context));
                        value.put("reply_text", commentString);

                        mDatabase.child("comments").child(commentToReply.getId()).child("replys").child(commentId).setValue(value);
                        mDatabase.child("comments").child(commentToReply.getId()).child("replys").child(commentId).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot replySnap) {

                                CommentReply reply = replySnap.getValue(CommentReply.class);
                                commentList.get(commentList.indexOf(commentToReply)).getChildList().add(reply);
                                Collections.sort(commentList.get(commentList.indexOf(commentToReply)).getChildList(), new Comparator<CommentReply>() {
                                    @Override
                                    public int compare(CommentReply o1, CommentReply o2) {
                                        return new Date(o1.getAdd_date()).compareTo(new Date(o2.getAdd_date()));
                                    }
                                });
                                setListData();
                                showDialog();

                                // send notification to post owner when there is a reply in a comment in that post
                                if (!postDetails.getUsername().equals(SharedPreferencesHelper.getUserName(context))) { //if commentor and the post owner is not the same person
                                    mDatabase.child("users").child(postDetails.getUsername()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            User user = dataSnapshot.getValue(User.class);
                                            // implement notification here
                                            String notificationText = "An user has replied on a comment at your post \n\"" + postDetails.getTitle() + "\"";
                                            SendRequestNotificationToUser(user, notificationText);
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                } else {
                                    hideDialog();
                                }

                                //if post owner and the comment owner is not the same person, because he is already getting a notification
                                if (!commentList.get(commentList.indexOf(commentToReply)).getUsername().equals(postDetails.getUsername())) {
                                    //if comment owner and replier is not same person
                                    if (!commentList.get(commentList.indexOf(commentToReply)).getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
                                        mDatabase.child("users").child(commentList.get(commentList.indexOf(commentToReply)).getUsername()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                User user = dataSnapshot.getValue(User.class);
                                                // implement notification here
                                                String notificationText = "An user has replied on your comment at the post \n\"" + postDetails.getTitle() + "\"";
                                                SendRequestNotificationToReplier(user, notificationText);
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                                    } else {
                                        hideDialog();
                                    }
                                } else {
                                    hideDialog();
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                hideDialog();
                                Toast.makeText(context, databaseError.toString(), Toast.LENGTH_LONG).show();
                            }
                        });

                    }


                }
            }
        });
        // repost to a post
        post_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetInfo.isOnline(context)) {
                    AlertMessage.showMessage(context, "Alert", AllStactic.netWarning);
                } else {
                    if (postDetails.getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
                        showPostDeleteConfirmationDialog();
                    } else {

                        Map<String, Object> value = new HashMap<>();
                        value.put(SharedPreferencesHelper.getUserName(context), ServerValue.TIMESTAMP);
                        mDatabase.child("post").child(postDetails.getId()).child("reports").setValue(value);
                        mDatabase.child("post").child(postDetails.getId()).child("reported").setValue(true);
                        Print.SnackBar(findViewById(R.id.parenView), "Post has been reported and will be reviewed shortly, Thank you!");
                    }
                }
            }
        });
        button_comment = (TextView) findViewById(R.id.button_comment);
        button_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_comment.setText("");
                isKeyboardOpenforReply = false;
                if (isKeyboardOpenforComment) {
                    hideKeyboard();
                    isKeyboardOpenforComment = false;
                } else {
                    showKeyboard();
                    isKeyboardOpenforComment = true;
                    et_comment.setHint("Write your comment");

                }
            }
        });

        // cahnge post image
        mDatabase.child("post").child(postDetails.getId()).child("image").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    postDetails.setPhoto(dataSnapshot.getValue().toString());
                }
                Print.log(dataSnapshot);
                if (!postDetails.getPhoto().equals("") && postDetails.getPhoto() != null) {

                    byte[] imageByteArray = Base64.decode(postDetails.getPhoto(), Base64.DEFAULT);
                    Glide.with(PostDetailsActivity.this).load(imageByteArray).placeholder(R.drawable.ic_launcher).crossFade().fitCenter().into(im_post);
                    imageByteArray = null;
                } else {
                    rl_image.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    /**
     * Dialog to show confirmation dialog before deactivating a post
     */
    private void showPostDeleteConfirmationDialog() {
        final Dialog dialog = new Dialog(PostDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.confirmation_dialog);

        TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message.setText("Are you sure want to delete the post? Deletion can not be undone");

        Button button_cancel = (Button) dialog.findViewById(R.id.button_cancel);
        Button button_ok = (Button) dialog.findViewById(R.id.button_ok);

        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDatabase.child("post").child(postDetails.getId()).child("published").setValue(false);
                Print.Toast(context, "Post has successfully deleted");
                finish();
            }
        });
        dialog.show();

    }


    public void hideKeyboard() {

        View view = PostDetailsActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(et_comment.getWindowToken(), 0);
        }
        rl_bottom_container.setVisibility(View.GONE);
    }

    public void showKeyboard() {
        rl_bottom_container.setVisibility(View.VISIBLE);
        et_comment.requestFocus();
        View view = PostDetailsActivity.this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void keyBoardControlForReply(Comment comment) {
        et_comment.setText("");
        isKeyboardOpenforComment = false;
        if (isKeyboardOpenforReply) {
            hideKeyboard();
            isKeyboardOpenforReply = false;
        } else {
            showKeyboard();
            isKeyboardOpenforReply = true;
            et_comment.setHint("@" + comment.getUsername() + "~");
            this.commentToReply = comment;
        }
    }

    /**
     * send notification to commentor
     * @param user
     * @param notificaionText
     */
    private void SendRequestNotificationToReplier(final User user, final String notificaionText) {

        String url = "https://fcm.googleapis.com/fcm/send";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "key=AAAAreSZh3k:APA91bEvRYHkS4lQzyeO7kt7-pZ_l7RrTbObQsuySFwLAeiVBoNwSQsA6t1UvuJSwwM5-pwab1MesAUYlHFHFAQ16gTNs6JDfuKAa131ZrKSVTIj_JzKcmBqa2-7vTvkUDA2IRv0vdvhDknbFm7JgtEt9ilm2mRexw");
                return headers;
            }

            @Override
            public byte[] getBody() {
                JSONObject jsonObject = new JSONObject();
                // JSONObject notification = new JSONObject();
                try {

                    jsonObject.put("to", user.getGcmid());
                    Gson gson = new Gson();
                    JSONObject data = new JSONObject();
                    String postDetailsToString = gson.toJson(postDetails);
                    data.put("keyname", postDetailsToString);
                    data.put("title", "Self-Help Hotline");
                    data.put("text", notificaionText);
//                    data.put("commentorName", SharedPreferencesHelper.getUserName(context));
                    data.put("notificaoionTo", user.getUsername());
                    jsonObject.put("data", data);

                } catch (Exception e) {
                }
                return jsonObject.toString().getBytes();
            }
        };

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Reply_notification_request");
    }

    /**
     * send notification to a post owner
     * @param user
     * @param notificaionText
     */
    private void SendRequestNotificationToUser(final User user, final String notificaionText) {
        String tag_json_obj = "json_obj_req";
        String url = "https://fcm.googleapis.com/fcm/send";

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
            }
        }) {

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "key=AAAAreSZh3k:APA91bEvRYHkS4lQzyeO7kt7-pZ_l7RrTbObQsuySFwLAeiVBoNwSQsA6t1UvuJSwwM5-pwab1MesAUYlHFHFAQ16gTNs6JDfuKAa131ZrKSVTIj_JzKcmBqa2-7vTvkUDA2IRv0vdvhDknbFm7JgtEt9ilm2mRexw");
                return headers;
            }

            @Override
            public byte[] getBody() {
                JSONObject jsonObject = new JSONObject();
                // JSONObject notification = new JSONObject();
                try {

                    jsonObject.put("to", user.getGcmid());
                    Gson gson = new Gson();
                    JSONObject data = new JSONObject();
                    String postDetailsToString = gson.toJson(postDetails);
                    data.put("keyname", postDetailsToString);
                    data.put("title", "Self-Help Hotline");
                    data.put("text", notificaionText);
//                    data.put("commentorName", SharedPreferencesHelper.getUserName(context));
                    data.put("notificaoionTo", user.getUsername());
                    jsonObject.put("data", data);

                } catch (Exception e) {
                }
                return jsonObject.toString().getBytes();
            }
        };

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
    }


    private void getComments() {
        mDatabase.child("comments").orderByChild("postId").equalTo(postDetails.getId()).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Comment comment = dataSnapshot.getValue(Comment.class);
                DataSnapshot votes = dataSnapshot.child("votes");

                for (DataSnapshot vote : votes.getChildren()) {
                    Vote like = vote.getValue(Vote.class);
                    int value = like.getLike();
                    if (value == -1) {
                        comment.setDownvote(comment.getDownvote() + 1);
                    } else if (value == 1) {
                        comment.setUpvote(comment.getUpvote() + 1);
                    }
                    if (vote.getKey().toString().equals(SharedPreferencesHelper.getUserName(context)) && value != 0) {
                        comment.setIsvoted(value);
                    }
                }


                if (comment.getDownvote() < 5) { // threshold to filter comment

                    // adding reply to comment
                    ArrayList<CommentReply> childList = new ArrayList<>();
                    childList.add(new CommentReply()); // one dummy element added to stop adapter from crashing
                    DataSnapshot relpys = dataSnapshot.child("replys");
                    for (DataSnapshot replySnap : relpys.getChildren()) {
                        CommentReply reply = replySnap.getValue(CommentReply.class);
                        childList.add(reply);
                        Collections.sort(childList, new Comparator<CommentReply>() {
                            @Override
                            public int compare(CommentReply o1, CommentReply o2) {
                                return new Date(o1.getAdd_date()).compareTo(new Date(o2.getAdd_date()));
                            }
                        });
                    }
                    comment.setChildObjectList(childList);

                    //adding comment to list
                    commentList.add(comment);
                    //sort commentlist with timestamp
                    Collections.sort(commentList, new Comparator<Comment>() {
                        @Override
                        public int compare(Comment o1, Comment o2) {
                            return new Date(o1.getAdd_date()).compareTo(new Date(o2.getAdd_date()));
                        }
                    });
                    setListData();

                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        setLikeUnlike();

    }

    private void setLikeUnlike() {
        tv_like.setText("" + postDetails.getLike());
        tv_unlike.setText("" + postDetails.getUnlike());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public void showDialog() {
        if (!pDialog.isShowing()) {
            pDialog.setMessage("Please wait ... ");
            pDialog.show();
        }
    }

    @Override
    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void setListData() {

        commentAdapter = new CommentAdapter(context, commentList);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setEmptyView(findViewById(R.id.list_empty));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.getRecycledViewPool().clear();

        recyclerView.setAdapter(commentAdapter);
        commentAdapter.notifyDataSetChanged();
        for (Comment com : commentList) {
            if (com.isExpanded()) {
                commentAdapter.expandParent(com);
            } else {
                commentAdapter.collapseParent(com);
            }

        }
    }

}
