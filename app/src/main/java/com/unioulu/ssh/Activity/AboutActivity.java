package com.unioulu.ssh.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.unioulu.ssh.R;
import com.unioulu.ssh.utility.BaseFunctions;

public class AboutActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    Context context;
    Toolbar toolbar;
    TextView version;
    TextView linktowebsite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_about);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        pDialog = new ProgressDialog(context);
        InitUi();


    }

    // intialize Ui elements
    private void InitUi() {
        version = (TextView) findViewById(R.id.version);
        version.setText(getVersion());
        linktowebsite = (TextView) findViewById(R.id.linktowebsite);
        linktowebsite.setOnClickListener(new View.OnClickListener() {

                                             @Override
                                             public void onClick(View v) {
                                                 Intent i = new Intent(Intent.ACTION_SEND);
                                                 i.setType("message/rfc822");
                                                 i.putExtra(Intent.EXTRA_EMAIL, new String[]{"asikrhmn@gmail.com"});
                                                 i.putExtra(Intent.EXTRA_SUBJECT, "Self-Help Hotline");
                                                 i.putExtra(Intent.EXTRA_TEXT, "");
                                                 try {
                                                     startActivity(Intent.createChooser(i, "Send feedback..."));
                                                 } catch (android.content.ActivityNotFoundException ex) {
                                                     Toast.makeText(context, "No email clients installed.", Toast.LENGTH_SHORT).show();
                                                 }
                                             }
                                         }
        );
    }

    /**
     * get current version of the application
     * @return
     */
    public String getVersion() {

        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        return version;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





}
