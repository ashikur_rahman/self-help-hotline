package com.unioulu.ssh.Notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.unioulu.ssh.Activity.PostDetailsActivity;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.Post;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.Date;

/**
 * Created by ashik on 06/02/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
//    private static final String commentNotification = "comment";
//    private static final String replyNotification = "reply";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Gson gson = new Gson();
        Post postDetails = gson.fromJson(remoteMessage.getData().get("keyname"), Post.class);
        String toUsername = remoteMessage.getData().get("notificaoionTo");
        if (SharedPreferencesHelper.isLogged(this) && SharedPreferencesHelper.getNotification(this) && toUsername.equals(SharedPreferencesHelper.getUserName(this))) {// if user is logged in , notification is enabled and is not self comment
//            if (remoteMessage.getData().get("notificationType").equals(commentNotification)) {
                Intent intent = new Intent(this, PostDetailsActivity.class);
                intent.putExtra("PostDetails", postDetails);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, m /* Request code */, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle(remoteMessage.getData().get("title"))
                        .setContentText(remoteMessage.getData().get("text"))
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(m, notificationBuilder.build());

//            } else if (remoteMessage.getData().get("notificationType").equals(replyNotification)) {
//
//            }
        }
    }

}