package com.unioulu.ssh.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.Activity.NewPostActivity;
import com.unioulu.ssh.R;
import com.unioulu.ssh.adapter.ViewPagerAdapter;
import com.unioulu.ssh.model.Category;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;

import java.util.ArrayList;


public class HomeFragment extends Fragment {
    Context context;
    ViewPager viewPager;
    TabLayout tabLayout;
    private DatabaseReference mDatabase;
    private ArrayList<Category> allCategory = new ArrayList<>();
    private String keyword;
    private String post_string;
    ImageView im_add_post;
    RelativeLayout rl_loader, rl_content;
    public EditText et_search;
    ViewPagerAdapter adapter;
    public static HomeFragment homeFragment;

    public static HomeFragment getHomeFragment() {
        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, null);
        context = getActivity();
        homeFragment = this;
        if (NetInfo.isOnline(context)) {
            mDatabase = FirebaseDatabase.getInstance().getReference();
            initUi(view);
        } else {
            AlertMessage.showMessage(context, "Error", AllStactic.netWarning);
        }

        return view;


    }

    private void initUi(View view) {
        rl_loader = (RelativeLayout) view.findViewById(R.id.rl_loader);
        rl_content = (RelativeLayout) view.findViewById(R.id.rl_content);

        im_add_post = (ImageView) view.findViewById(R.id.im_add_post);
        et_search = (EditText) view.findViewById(R.id.et_search);
        im_add_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewPostActivity.class);
                startActivity(intent);
            }
        });

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        Category categry;
        categry = new Category();
        categry.setTitle("All Posts");
        categry.setSearchKey("");
        allCategory.add(categry);
        mDatabase.child("category").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Category categry;
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    categry = postSnapshot.getValue(Category.class);
                    categry.setSearchKey(categry.getTitle());
                    allCategory.add(categry);
                }
                setupViewPager(viewPager);
                rl_loader.setVisibility(View.GONE);
                rl_content.setVisibility(View.VISIBLE);
                tabLayout.post(mTabLayout_config);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Print.Toast(context, databaseError.toString());
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                et_search.setText("");
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (adapter != null) {
                    PostByCategoryFragment selectedFragment = (PostByCategoryFragment) adapter.getItem(viewPager.getCurrentItem());
                    selectedFragment.searchActivity(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), allCategory);
        viewPager.setAdapter(adapter);

    }

    Runnable mTabLayout_config = new Runnable() {
        @Override
        public void run() {

            if (tabLayout.getWidth() < getActivity().getResources().getDisplayMetrics().widthPixels) {
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                ViewGroup.LayoutParams mParams = tabLayout.getLayoutParams();
                mParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                tabLayout.setLayoutParams(mParams);
            } else {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
    };

}
