package com.unioulu.ssh.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.unioulu.ssh.Activity.NewPostActivity;
import com.unioulu.ssh.R;
import com.unioulu.ssh.adapter.ViewPagerAdapter;
import com.unioulu.ssh.model.Category;
import com.unioulu.ssh.utility.AlertMessage;
import com.unioulu.ssh.utility.AllStactic;
import com.unioulu.ssh.utility.NetInfo;
import com.unioulu.ssh.utility.Print;

import java.util.ArrayList;


public class SearchResultFragment extends Fragment {
    Context context;
    ViewPager viewPager;
    TabLayout tabLayout;
    private DatabaseReference mDatabase;
    private Spinner category_list;
    private ArrayList<Category> allCategory = new ArrayList<>();
    ImageView im_add_post;
    RelativeLayout rl_loader, rl_content;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, null);
        context = getActivity();
        if (NetInfo.isOnline(context)) {
            mDatabase = FirebaseDatabase.getInstance().getReference();
            initUi(view);

        } else {
            AlertMessage.showMessage(context, "Error", AllStactic.netWarning);
        }

        return view;


    }

    private void initUi(View view) {
        rl_loader = (RelativeLayout) view.findViewById(R.id.rl_loader);
        rl_content = (RelativeLayout) view.findViewById(R.id.rl_content);

        im_add_post = (ImageView) view.findViewById(R.id.im_add_post);
        im_add_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, NewPostActivity.class);
                startActivity(intent);
            }
        });
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.post(mTabLayout_config);
        category_list = (Spinner) view.findViewById(R.id.category_list);
        mDatabase.child("category").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Category categry;
                categry = new Category();
                categry.setTitle("All Category");
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    categry = postSnapshot.getValue(Category.class);
                    categry.setId(Integer.parseInt(postSnapshot.getKey()));
                    allCategory.add(categry);
                }
                setupViewPager(viewPager);
                PopulateCategorySpinner();
                rl_loader.setVisibility(View.GONE);
                rl_content.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Print.Toast(context, databaseError.toString());
            }
        });

    }

    public void PopulateCategorySpinner() {
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(context, android.R.layout.simple_spinner_item, allCategory);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        category_list.setAdapter(adapter);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager(), allCategory);
        viewPager.setAdapter(adapter);

    }

    Runnable mTabLayout_config = new Runnable() {
        @Override
        public void run() {

            if (tabLayout.getWidth() < getActivity().getResources().getDisplayMetrics().widthPixels) {
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                ViewGroup.LayoutParams mParams = tabLayout.getLayoutParams();
                mParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                tabLayout.setLayoutParams(mParams);
            } else {
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            }
        }
    };

}
