package com.unioulu.ssh.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.unioulu.ssh.R;
import com.unioulu.ssh.adapter.PostAdapter;
import com.unioulu.ssh.model.Category;
import com.unioulu.ssh.model.Vote;
import com.unioulu.ssh.model.Post;

import com.unioulu.ssh.utility.RecyclerViewEmptySupport;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by ashik on 01/01/17.
 */

public class PostByCategoryFragment extends Fragment {
    private static final String DESCRIBABLE_KEY = "describable_key";
    Category category;
    private RecyclerViewEmptySupport recyclerView;
    PostAdapter postAdapter;
    List<Post> searchPostList = new ArrayList<>();
    List<Post> allPostList = new ArrayList<>();

    private DatabaseReference mDatabase;
    Context context;
    RecyclerViewEmptySupport.LayoutManager mLayoutManager;
    boolean firstLoad = true;

    public PostByCategoryFragment() {
        // Required empty public constructor
    }

    public static PostByCategoryFragment newInstance(Category describable) {
        PostByCategoryFragment fragment = new PostByCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(DESCRIBABLE_KEY, describable);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_by_category, container, false);
        category = (Category) getArguments().getSerializable(
                DESCRIBABLE_KEY);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        context = getActivity().getApplicationContext();
        recyclerView = (RecyclerViewEmptySupport) view.findViewById(R.id.recycler_view);
        recyclerView.setEmptyView(view.findViewById(R.id.list_empty));

        setListData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getPostsByCategory();

    }

    private void getPostsByCategory() {
        allPostList.clear();
        searchPostList.clear();
        Query query;
        if (category.getSearchKey().equals("")) {
            query = mDatabase.child("post");
        } else {
            query = mDatabase.child("post").orderByChild("category").equalTo(category.getId());
        }

        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Post post = dataSnapshot.getValue(Post.class);
                DataSnapshot votes = dataSnapshot.child("votes");

                for (DataSnapshot vote : votes.getChildren()) {

                    Vote like = vote.getValue(Vote.class);

                    if (like.getLike() == -1) {
                        post.setUnlike(post.getUnlike() + 1);
                    } else if (like.getLike() == 1) {
                        post.setLike(post.getLike() + 1);
                    }
                    if (vote.getKey().toString().equals(SharedPreferencesHelper.getUserName(context)) && like.getLike() != 0) {
                        post.setIsliked(like.getLike());
                    }
                }
                if (post.getUnlike() < 5 && post.isPublished()) {
                    allPostList.add(post);
                    Collections.sort(allPostList, new Comparator<Post>() {
                        @Override
                        public int compare(Post o1, Post o2) {
                            return new Date(o2.getAdd_date()).compareTo(new Date(o1.getAdd_date()));
                        }
                    });
                    String sss = HomeFragment.getHomeFragment().et_search.getText().toString().trim();
                    if (sss.equals("")) {
                        searchPostList.add(post);
                        Collections.sort(searchPostList, new Comparator<Post>() {
                            @Override
                            public int compare(Post o1, Post o2) {
                                return new Date(o2.getAdd_date()).compareTo(new Date(o1.getAdd_date()));
                            }
                        });
                    } else {
                        // if user is already in search mode and there is a new incoming post filter it according to search string
                        if (post.getTitle().toLowerCase().contains(sss.toLowerCase())) {
                            searchPostList.add(post);
                            Collections.sort(searchPostList, new Comparator<Post>() {
                                @Override
                                public int compare(Post o1, Post o2) {
                                    return new Date(o2.getAdd_date()).compareTo(new Date(o1.getAdd_date()));
                                }
                            });
                        }
                    }
                    postAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void searchActivity(String searchString) {
        searchPostList.clear();
        for (int i = 0; i < allPostList.size(); i++) {
            if (allPostList.get(i).getTitle().toLowerCase().contains(searchString.toLowerCase())) {
                searchPostList.add(allPostList.get(i));
            }
        }

        postAdapter.notifyDataSetChanged();
    }

    private void setListData() {
        postAdapter = new PostAdapter(searchPostList);
        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postAdapter);
    }
}
