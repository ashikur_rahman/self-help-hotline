package com.unioulu.ssh.utility;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DateUtil {

    public static String relativeDate(long duration) {
        Date now = new Date();
        Date date = new Date(duration);
        if (date.before(now)) {
            int days_passed = (int) TimeUnit.MILLISECONDS.toDays(now.getTime() - date.getTime());
            if (days_passed > 0) return days_passed + " days ago";
            else {
                int hours_passed = (int) TimeUnit.MILLISECONDS.toHours(now.getTime() - date.getTime());
                if (hours_passed > 0) return hours_passed + " hours ago";
                else {
                    int minutes_passed = (int) TimeUnit.MILLISECONDS.toMinutes(now.getTime() - date.getTime());
                    if (minutes_passed > 0) return minutes_passed + " minutes ago";
                    else {
                        int seconds_passed = (int) TimeUnit.MILLISECONDS.toSeconds(now.getTime() - date.getTime());
                        return seconds_passed + " seconds ago";
                    }
                }
            }

        } else {
            return new SimpleDateFormat("HH:mm:ss MM/dd/yyyy").format(date).toString();
        }
    }

    public static String convertTime(long time) {
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(date);
    }

    public static String GetCurrentDateWithTime() {

        Date d = new Date();

        SimpleDateFormat ft = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a",
                Locale.ENGLISH);

        CharSequence s = ft.format(d);
        return s.toString();
    }
}
