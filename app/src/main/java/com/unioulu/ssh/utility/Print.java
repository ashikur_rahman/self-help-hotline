package com.unioulu.ssh.utility;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.unioulu.ssh.R;

import java.util.HashMap;
import java.util.Map;

public class Print {

    public static <E> void Toast(Context context, E msg) {

        Toast.makeText(context, "" + msg.toString(), Toast.LENGTH_LONG).show();

    }

    public static <E> void SnackBar(View parentView, E msg) {

        Snackbar.make(parentView, "" + msg.toString(), Snackbar.LENGTH_LONG).show();

    }

    public static <E> void loge(String tag, E[] array) {
        for (E element : array) {
            Log.d("" + tag, "" + element);
        }
    }

    public static <E> void logw(String tag, E[] array) {
        for (E element : array) {
            Log.w("" + tag, "" + element);
        }
    }

    public static <E> void loge(String tag, E element) {
        Log.d("" + tag, "" + element);
    }

    public static <E> void log(E msg) {
        Log.w("tag", "" + msg);

    }

    public static <E> void logMap(Map<E, E> params) {
        for (Map.Entry<E, E> entry : params.entrySet()) {
            Log.w(entry.getKey().toString(), ":" + entry.getValue().toString());
        }
    }

    public static void longmsg(String TAG, String message) {
        int maxLogSize = 2000;
        for (int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            Log.w(TAG, message.substring(start, end));
        }
    }

}
