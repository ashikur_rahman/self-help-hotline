package com.unioulu.ssh.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


public final class SharedPreferencesHelper {

    private static final String PREFS_FILE_NAME = "selp_help_hotline";


    public static void logOut(Context c) {

        final SharedPreferences prefs = c.getSharedPreferences(PREFS_FILE_NAME,
                Context.MODE_PRIVATE);
        prefs.edit().putBoolean("LOGIN", false).commit();
    }

    public static void setLogin(Context c) {

        final SharedPreferences prefs = c.getSharedPreferences(PREFS_FILE_NAME,
                Context.MODE_PRIVATE);
        prefs.edit().putBoolean("LOGIN", true).commit();
    }

    public static boolean isLogged(Context c) {

        final SharedPreferences prefs = c.getSharedPreferences(PREFS_FILE_NAME,
                Context.MODE_PRIVATE);

        return prefs.getBoolean("LOGIN", false);
    }

    //
    public static String getEmail(final Context ctx) {
        return (ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE).getString("email", ""));

    }

    public static void setEmail(final Context ctx, final String data) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final Editor editor = prefs.edit();
        editor.putString("email", data);
        editor.commit();
    }

    public static String getUserName(final Context ctx) {
        return (ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE).getString("username", ""));

    }

    public static void setUserName(final Context ctx, final String data) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final Editor editor = prefs.edit();
        editor.putString("username", data);
        editor.commit();
    }

    public static String getPassword(final Context ctx) {
        return (ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE).getString("password", ""));

    }

    public static void setPassword(final Context ctx, final String data) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final Editor editor = prefs.edit();
        editor.putString("password", data);
        editor.commit();
    }

    public static String getGcmToken(final Context ctx) {
        return (ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE).getString("gcmtoken", ""));

    }

    public static void setGcmToken(final Context ctx, final String data) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final Editor editor = prefs.edit();
        editor.putString("gcmtoken", data);
        editor.commit();
    }

    public static boolean getNotification(final Context ctx) {
        return (ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE).getBoolean("notification", true));
    }

    public static void setNotification(final Context ctx, final boolean data) {
        final SharedPreferences prefs = ctx.getSharedPreferences(
                PREFS_FILE_NAME, Context.MODE_PRIVATE);
        final Editor editor = prefs.edit();
        editor.putBoolean("notification", data);
        editor.commit();
    }
}
