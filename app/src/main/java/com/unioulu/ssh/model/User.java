package com.unioulu.ssh.model;


import java.io.Serializable;

/**
 * Created by ashik on 08/12/16.
 */

public class User implements Serializable {

    private String email;
    private String password;
    private String username;
    private String gcmid;

    public User(String email, String password, String username, String gcmid) {
        this.email = email;
        this.password = password;
        this.username = username;
        this.gcmid = gcmid;
    }

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGcmid() {
        return gcmid;
    }

    public void setGcmid(String gcmid) {
        this.gcmid = gcmid;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", gcmid='" + gcmid + '\'' +
                '}';
    }
}
