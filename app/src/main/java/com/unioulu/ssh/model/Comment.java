package com.unioulu.ssh.model;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.io.Serializable;
import java.util.List;

/**
 * Created by gorinds on 14/01/2017.
 */

public class Comment implements Serializable, Parent<CommentReply> {
    private String id;
    boolean is_best_answer;
    private String comment;
    private String username;
    private int downvote = 0;
    private int upvote = 0;
    private long add_date;
    private String postId;
    private int isvoted = 0;
    private boolean isExpanded = false;

    private List<CommentReply> mChildrenList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean is_best_answer() {
        return is_best_answer;
    }

    public void setIs_best_answer(boolean is_best_answer) {
        this.is_best_answer = is_best_answer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getDownvote() {
        return downvote;
    }

    public void setDownvote(int downvote) {
        this.downvote = downvote;
    }

    public int getUpvote() {
        return upvote;
    }

    public void setUpvote(int upvote) {
        this.upvote = upvote;
    }

    public long getAdd_date() {
        return add_date;
    }

    public void setAdd_date(long add_date) {
        this.add_date = add_date;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public int getIsvoted() {
        return isvoted;
    }

    public void setIsvoted(int isvoted) {
        this.isvoted = isvoted;
    }


    @Override
    public String toString() {
        return "Comment{" +
                "id='" + id + '\'' +
                ", is_best_answer=" + is_best_answer +
                ", comment='" + comment + '\'' +
                ", username='" + username + '\'' +
                ", downvote=" + downvote +
                ", upvote=" + upvote +
                ", add_date=" + add_date +
                ", postId='" + postId + '\'' +
                ", isvoted=" + isvoted +
                ", isExpanded=" + isExpanded +
                ", mChildrenList=" + mChildrenList +
                '}';
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    @Override
    public List<CommentReply> getChildList() {
        return mChildrenList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    public void setChildObjectList(List<CommentReply> list) {
        mChildrenList = list;

    }
}
