package com.unioulu.ssh.model;

import java.io.Serializable;

/**
 * Created by ashik on 31/12/16.
 */

public class Vote implements Serializable {
    private int like;


    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
