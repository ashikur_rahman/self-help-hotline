package com.unioulu.ssh.model;

import java.io.Serializable;

/**
 * Created by ashik on 31/12/16.
 */

public class Category implements Serializable {
    private int id = 0;
    private String title;
    private String searchKey;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public String toString() {
        return title;
    }
}
