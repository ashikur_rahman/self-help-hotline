package com.unioulu.ssh.model;

import java.util.Date;

/**
 * Created by ashik on 10/02/17.
 */


public class CommentReply {

    private String id = "";
    private long add_date;
    private String username;
    private String reply_text;

    public CommentReply(String id) {
        this.id = id;
    }

    public CommentReply() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getAdd_date() {
        return add_date;
    }

    public void setAdd_date(long add_date) {
        this.add_date = add_date;
    }

    public String getReply_text() {
        return reply_text;
    }

    public void setReply_text(String reply_text) {
        this.reply_text = reply_text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}