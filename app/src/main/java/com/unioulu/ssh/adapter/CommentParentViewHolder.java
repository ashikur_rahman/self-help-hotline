package com.unioulu.ssh.adapter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.Comment;

/**
 * Created by ashik on 10/02/17.
 */

public class CommentParentViewHolder extends ParentViewHolder {
    public TextView comment_text, comment_username, comment_upvote, comment_downvote, comment_timestamp, number_of_reply;
    public ImageButton im_upvote, im_downvote;
    public LinearLayout ll_reply;
    public ImageView mArrowExpandImageView;
    LinearLayout parent;

    public CommentParentViewHolder(@NonNull View view) {
        super(view);
        parent = (LinearLayout) view.findViewById(R.id.parent);
        comment_username = (TextView) view.findViewById(R.id.comment_username);
        comment_text = (TextView) view.findViewById(R.id.comment_text);
        comment_upvote = (TextView) view.findViewById(R.id.comment_upvote_amount);
        comment_downvote = (TextView) view.findViewById(R.id.comment_downvote_amount);
        comment_timestamp = (TextView) view.findViewById(R.id.comment_timestamp);
        im_upvote = (ImageButton) view.findViewById(R.id.im_upvote);
        im_downvote = (ImageButton) view.findViewById(R.id.im_downvote);
        ll_reply = (LinearLayout) view.findViewById(R.id.ll_reply);
        number_of_reply = (TextView) view.findViewById(R.id.number_of_reply);
        mArrowExpandImageView = (ImageView) itemView.findViewById(R.id.parent_list_item_expand_arrow);
    }

    public void collapseItem() {
        collapseView();
    }

    public void expandItem() {
        expandView();
    }

    @Override
    public boolean shouldItemViewClickToggleExpansion() {
        return false;
    }
}
