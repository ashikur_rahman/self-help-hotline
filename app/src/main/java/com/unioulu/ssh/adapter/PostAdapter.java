package com.unioulu.ssh.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.unioulu.ssh.Activity.EditPostActivity;
import com.unioulu.ssh.Activity.PostDetailsActivity;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.Post;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.List;

/**
 * Created by ashik on 02/01/17.
 */


public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {

    private List<Post> postList;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_post_title, tv_post_details;

        public ViewHolder(View view) {
            super(view);
            context = view.getContext();

            tv_post_title = (TextView) view.findViewById(R.id.tv_post_title);
            tv_post_details = (TextView) view.findViewById(R.id.tv_post_details);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, PostDetailsActivity.class);
                    intent.putExtra("PostDetails", postList.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    if (SharedPreferencesHelper.getUserName(context).equals(postList.get(getAdapterPosition()).getUsername())) {
                        Intent intent = new Intent(context, EditPostActivity.class);
                        intent.putExtra("PostDetails", postList.get(getAdapterPosition()));
                        context.startActivity(intent);
                    }
                    return false;
                }
            });
        }
    }


    public PostAdapter(List<Post> postList) {
        this.postList = postList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_post, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Post post = postList.get(position);
        holder.tv_post_title.setText(post.getTitle());
        holder.tv_post_details.setText(post.getDetails());
    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}