package com.unioulu.ssh.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.unioulu.ssh.fragment.PostByCategoryFragment;
import com.unioulu.ssh.model.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashik on 01/01/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    ArrayList<Category> allCategory = null;
    private final List<Fragment> mFragmentList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager, ArrayList<Category> allCategory) {
        super(manager);
        this.allCategory = allCategory;
        for (Category category :
                allCategory) {
            PostByCategoryFragment fragment = PostByCategoryFragment.newInstance(category);
            addFragment(fragment);
        }
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return allCategory.size();
    }

    public void addFragment(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return allCategory.get(position).getTitle();
    }
}
