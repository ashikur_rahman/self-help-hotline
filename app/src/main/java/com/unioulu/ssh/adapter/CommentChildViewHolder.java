package com.unioulu.ssh.adapter;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.unioulu.ssh.R;

/**
 * Created by ashik on 10/02/17.
 */

public class CommentChildViewHolder extends ChildViewHolder {
    TextView reply_username, reply_timestamp, reply_text;
    RelativeLayout parent;

    public CommentChildViewHolder(@NonNull View itemView) {
        super(itemView);
        reply_username = (TextView) itemView.findViewById(R.id.reply_username);
        reply_timestamp = (TextView) itemView.findViewById(R.id.reply_timestamp);
        reply_text = (TextView) itemView.findViewById(R.id.reply_text);

        parent = (RelativeLayout) itemView.findViewById(R.id.parent);
    }
}
