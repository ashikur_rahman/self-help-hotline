package com.unioulu.ssh.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.unioulu.ssh.Activity.PostDetailsActivity;
import com.unioulu.ssh.R;
import com.unioulu.ssh.model.Comment;
import com.unioulu.ssh.model.CommentReply;
import com.unioulu.ssh.utility.DateUtil;
import com.unioulu.ssh.utility.Print;
import com.unioulu.ssh.utility.SharedPreferencesHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashik on 02/01/17.
 */


public class CommentAdapter extends ExpandableRecyclerAdapter<Comment, CommentReply, CommentParentViewHolder, CommentChildViewHolder> {

    private List<Comment> postList;
    private Context context;
    LayoutInflater mInflater;

    private static final int empty_item = 2;
    private static final int not_empty_item = 3;


    public CommentAdapter(Context context, List<Comment> parentItemList) {
        super(parentItemList);
        mInflater = LayoutInflater.from(context);
        postList = parentItemList;
        this.context = context;

    }

    @Override
    public int getChildViewType(int parentPosition, int childPosition) {
        CommentReply ingredient = postList.get(parentPosition).getChildList().get(childPosition);
        if (ingredient.getId().equals("")) {
            return empty_item;
        } else {
            return not_empty_item;
        }
    }


    @NonNull
    @Override
    public CommentParentViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView = mInflater.inflate(R.layout.item_comment, parentViewGroup, false);
        return new CommentParentViewHolder(recipeView);
    }

    @NonNull
    @Override
    public CommentChildViewHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {

        View ingredientView;
        switch (viewType) {
            default:
            case empty_item:
                ingredientView = mInflater.inflate(R.layout.item_comment_child_empty, childViewGroup, false);
                break;
            case not_empty_item:
                ingredientView = mInflater.inflate(R.layout.item_comment_child, childViewGroup, false);
                break;
        }
        return new CommentChildViewHolder(ingredientView);
    }

    @Override
    public void onBindParentViewHolder(@NonNull final CommentParentViewHolder holder, final int position, @NonNull final Comment comment) {

        holder.comment_username.setText(comment.getUsername());
        holder.comment_text.setText(comment.getComment());
        holder.comment_upvote.setText("" + comment.getUpvote());
        holder.comment_downvote.setText("" + comment.getDownvote());
        holder.comment_timestamp.setText(DateUtil.relativeDate(comment.getAdd_date()));
        if (comment.getChildList().size() < 3) {
            holder.number_of_reply.setText(comment.getChildList().size() - 1 + " reply");
        } else {
            holder.number_of_reply.setText(comment.getChildList().size() - 1 + " replies");
        }
        if (comment.getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
            holder.im_upvote.setAlpha(.2f);
            holder.im_downvote.setAlpha(.2f);
        } else {
            if (comment.getIsvoted() == 0) {
                holder.im_upvote.setAlpha(.2f);
                holder.im_downvote.setAlpha(.2f);
            } else if (comment.getIsvoted() == 1) {
                holder.im_upvote.setAlpha(1f);
                holder.im_downvote.setAlpha(.2f);
            } else if (comment.getIsvoted() == -1) {
                holder.im_upvote.setAlpha(.2f);
                holder.im_downvote.setAlpha(1f);
            }
        }


        holder.im_upvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
                    Print.SnackBar(PostDetailsActivity.getPostDetailsActivity().findViewById(R.id.parenView), "You can't vote on your own comment");
                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                    Map<String, Object> value = new HashMap<>();
                    if (comment.getIsvoted() == 1) {
                        value.put("like", 0);
                        comment.setIsvoted(0);
                        comment.setUpvote(comment.getUpvote() - 1);
                        holder.im_downvote.setAlpha(.2f);
                        holder.im_upvote.setAlpha(.2f);
                    } else {
                        value.put("like", 1);
                        holder.im_downvote.setAlpha(.2f);
                        holder.im_upvote.setAlpha(1f);
                        comment.setUpvote(comment.getUpvote() + 1);
                        if (comment.getIsvoted() == -1) {
                            comment.setDownvote(comment.getDownvote() - 1);
                        }
                        comment.setIsvoted(1);

                    }
                    mDatabase.child("comments").child(comment.getId()).child("votes").child(SharedPreferencesHelper.getUserName(context)).setValue(value);
                    postList.set(position, comment);

                }
                CommentAdapter.this.notifyDataSetChanged();
            }
        });
        holder.im_downvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (comment.getUsername().equals(SharedPreferencesHelper.getUserName(context))) {
                    Print.SnackBar(PostDetailsActivity.getPostDetailsActivity().findViewById(R.id.parenView), "You can't vote on your own comment");
                } else {
                    DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
                    Map<String, Object> value = new HashMap<>();
                    if (comment.getIsvoted() == -1) {
                        value.put("like", 0);
                        comment.setIsvoted(0);
                        holder.im_downvote.setAlpha(.2f);
                        holder.im_upvote.setAlpha(.2f);
                        comment.setDownvote(comment.getDownvote() - 1);

                    } else {
                        value.put("like", -1);
                        holder.im_downvote.setAlpha(1f);
                        holder.im_upvote.setAlpha(.2f);
                        comment.setDownvote(comment.getDownvote() + 1);
                        if (comment.getIsvoted() == 1) {
                            comment.setUpvote(comment.getUpvote() - 1);
                        }
                        comment.setIsvoted(-1);
                    }
                    mDatabase.child("comments").child(comment.getId()).child("votes").child(SharedPreferencesHelper.getUserName(context)).setValue(value);
                    postList.set(position, comment);
                }
                CommentAdapter.this.notifyDataSetChanged();

            }
        });
        holder.ll_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostDetailsActivity.getPostDetailsActivity().keyBoardControlForReply(comment);
            }
        });

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.isExpanded()) {
                    holder.mArrowExpandImageView.setImageResource(R.drawable.plus);
                    holder.collapseItem();
                    PostDetailsActivity.getPostDetailsActivity().commentList.get(position).setExpanded(false);
                } else {
                    holder.mArrowExpandImageView.setImageResource(R.drawable.minus);
                    holder.expandItem();
                    PostDetailsActivity.getPostDetailsActivity().commentList.get(position).setExpanded(true);

                }
            }
        });
        if (comment.isExpanded()) {
            holder.mArrowExpandImageView.setImageResource(R.drawable.minus);
        } else {
            holder.mArrowExpandImageView.setImageResource(R.drawable.plus);
        }
    }

    @Override
    public void onBindChildViewHolder(@NonNull CommentChildViewHolder childViewHolder, int parentPosition, int childPosition, @NonNull CommentReply child) {
        if (!child.getId().equals("")) {
            childViewHolder.reply_username.setText(child.getUsername());
            childViewHolder.reply_timestamp.setText(DateUtil.relativeDate(child.getAdd_date()));
            childViewHolder.reply_text.setText(child.getReply_text());

        }
    }

}